<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function index()
    {
        return view('index');
    }
    public function submit(Request $request)
    {
        $request->validate([
			'gambar' => 'required'
		]);

        $image = $request->file('gambar');
        $image->move('data', $image->getClientOriginalName());

        if($request->hasFile('sertifikat')){
            $certificate = $request->file('sertifikat');
            $certificate->move('data', $certificate->getClientOriginalName());
        }

        if($request->hasFile('cv')){
            $cv = $request->file('cv');
            $cv->move('data', $cv->getClientOriginalName());
        }
        
        print_r('upload sukses');
    }
}
