<!DOCTYPE html>
<html>
<head>
	<title>Input data mahasiswa</title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>

<body>
	<form action="{{url('/submit')}}" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="container">
			<h1 class="text-center">Input data mahasiswa</h1>
			<div class="form-group">
				<label>Nama Lengkap</label>
				<input type="text" class="form-control" placeholder="Masukkan nama lengkap">
			</div>
			<label>Gender</label>
			<br>
			<div class="form-check form-check-inline">
				<input class="form-check-input" type="radio" name="genderRadio" checked>
				<label class="form-check-label">Laki laki</label>
			</div>
			<div class="form-check form-check-inline">
				<input class="form-check-input" type="radio" name="genderRadio">
				<label class="form-check-label">Perempuan</label>
			</div>
			<div class="form-group">
				<br>
				<label>Tempat tanggal lahir</label>
				<input type="text" class="form-control" placeholder="Masukkan tempat tanggal lahir">
			</div>
			<div class="form-group">
				<label>Upload gambar</label>
				<input type="file" class="form-control-file" id="uploadGambar" name="gambar">
			</div>
			<div class="form-group">
				<label>Upload attachement sertifikat</label>
				<input type="file" class="form-control-file" id="uploadSertifikat" name="sertifikat">
			</div>
			<div class="form-group">
				<label>Upload CV</label>
				<input type="file" class="form-control-file" id="uploadCv" name="cv">
			</div>
			<button type="button" id="submitButton" class="btn btn-primary">Submit</button>
			<button type="button" id="resetButton" class="btn btn-danger">Reset</button>
		</div>
	</form>
</body>

<script>
$('#submitButton').click(function(){
	if($('#uploadGambar').get(0).files.length == 0){
		alert('File gambar belum di masukkan');
	}
	else{
		$('form').submit();
	}
});

$('#resetButton').click(function(){
	$('form')[0].reset();
});

$("#uploadGambar").change(function () {
	var fileExtension = ['jpg','jpeg','png'];
	if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
		alert("Format tidak sesuai, tolong upload file dengan format : "+fileExtension.join(', '));
		$(this).val(''); 
	}
});

$("#uploadSertifikat").change(function () {
	var fileExtension = ['zip','rar'];
	if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
		alert("Format tidak sesuai, tolong upload file dengan format : "+fileExtension.join(', '));
		$(this).val(''); 
	}
});

$("#uploadCv").change(function () {
	var fileExtension = ['pdf'];
	if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
		alert("Format tidak sesuai, tolong upload file dengan format : "+fileExtension.join(', '));
		$(this).val(''); 
	}
});
</script>
</html>